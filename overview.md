[//]: # (overview.md)

Overview
====

Context
----

Terminals and terminal emulators work on the model of a character
matrix with one character per matrix cell.

Terminals fall into three categories, those that actually put a single
character into each matrix cell, like the console provided by the
Linux kernel, those that use graphical font technology to put a
character cluster into each cell, but do not do shaping or bidi
reordering, like most X11 terminals, and those that use the full
capabilities of the graphical font system and also do shaping and
bidi, like the macOS Terminal.app.

The last category can not easily be made to work for a full-screen CUI
application, because the actual placement of the characters on each
line is effectively unpredictable.  To support a full-screen CUI
application, the terminal should not do bidi reordering and should not
implement ligatures on its own.  These features must be implemented by
the application and communicated through precomposed characters and
presentation forms.

General Guidelines
----

The special limitation of a simple character matrix implementation
(the first category) is that it does not support non-spacing marks
(diacritics).  All marks must be precomposed with their base character
by the application, or dropped.  A terminal that uses a graphical font
system OTOH can implement non-spacing marks, but those must than be
ignored by the shaping process inside the application.

Keep it very simple.  We want the common denominator.  If any of the
precomposed forms required by the script are not handled by the font,
we can not do anything more about it.  OTOH we should try not to
require anything that goes beyond the mandatory rules for ligatures
for a script, because that is not worth the risk, especially because
on a terminal we expect a monospaced font, and those will not have
special ligature forms beyond the basics anyway.

We ignore diacritics that are not covered as ligatures.  The Linux
console does not support non-spacing marks at all, so here we remove
all diacritics.  On all other terminals we ignore the diacritics
(mostly, they still get dropped within a ligature) and expect the
terminal to render them correctly.  We do not try to detect the third
category of terminals, because they are broken with or without this
code.

Selected Details
----

At this point, the required data tables are generated from the Unicode
data as given by Emacs.  This implies that only glyphs are used that
have precomposed forms in Unicode.  Currently only Arabic is handled.

Ligatures are handled by looking at each pair of characters in turn
and doing something like:

        if ligable curr next
            ligate curr next

Any diacritics between ``curr`` and ``next`` are removed, because it
is not clear what to do about them as a semantic matter and also
because I do not understand how to do reordering of glyphs with the
given data structures in Emacs yet.

Shaping is handled by doing this for each character pair in the text:

        if next-shapable curr and prev-shapable next
            next-shape curr
            prev-shape next

Diacritics are ignored during this process.

Future
----

In an extended version, a selection of more ligatures could be
configurable by the user.  Probably that will just require the data
tables to be extended.

For scripts where Unicode does not provide precomposed forms, those
forms would have to go into the PUA, and the font and the code to
generate the data structures would than have to be written to use
these PUA allocations.

----
[//]: # (eof)
