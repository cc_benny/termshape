;;------------------------------------------------------------------------
;;      termshape.el
;;
;;      An Emacs package to do Arabic shaping in CUA mode (emacs -nw)
;;      on the Linux console and in X11 terminal windows.  See
;;      overview.md for more details.
;;------------------------------------------------------------------------
;;      This file is part of Termshape.
;;
;;      Termshape is free software: You can redistribute it and/or
;;      modify it under the terms of the GNU General Public License as
;;      published by the Free Software Foundation, either version 3 of
;;      the License, or (at your option) any later version.
;;
;;      You should have received a copy of the GNU General Public
;;      License along with Termshape in the file COPYING.  If not, see
;;      <https://www.gnu.org/licenses/>.
;;------------------------------------------------------------------------

(require 'cl-lib)  ;; cl-loop

;;; A debug macro.

;; Replace with nothing in production mode.
(defsubst termshape-debug (tag gstring))

;; Uncomment the next block and re-evaluate the buffer (M-x
;; eval-buffer) to get debug info stored into an internal variable
;; when this code is called during redisplay.  Afterwards the debug
;; info can be shown (e.g. in the scratch buffer) using "(insert
;; (termshape-debug-to-string))".

;; (defsubst termshape-debug (tag gstring)
;;   (termshape-debug-log tag gstring))
;; (load (expand-file-name "tshape-debug.el" "."))

;;; Initialize the data structures.

;; This is an internal constant, just so that we can have a single
;; function to generate the data, but than distribute the result into
;; several better documented constants that are derived from this.
(defconst termshape-data
  (eval-when-compile
    ;; (expand-file-name ... "."): Creates an absolute file name which
    ;; is what `load' needs to find the file.  We do not use `require'
    ;; here, because that only works when this package is installed as
    ;; a regular package.
    (load (expand-file-name "tshape-gen.el" "."))
    (termshape-make-tables)))

(defconst termshape-arabic-chars (nth 0 termshape-data)
  "A char-table that contains not-nil for each Arabic character.")

(defconst termshape-ligatures (nth 1 termshape-data)
  "Define which ligatures we know about.
This is a tree made from char-tables.  It has only two levels,
see `termshape-ligate' and `termshape-make-ligature'.  Longer
ligatures can be created by chaining the two-character form.")

(defconst termshape-shapes-connect-next (nth 2 termshape-data)
  "For a character, point to a version that connects forward.")

(defconst termshape-shapes-connect-prev (nth 3 termshape-data)
  "For a character, point to a version that connects backward.")

;;; Some more lgstring / glyph primitives.

;; See composite.el for more primitives like this and the docstring of
;; `composition-get-gstring' for documentation of the data structures
;; for glyphs and glyph strings.

(defsubst lglyph-termglyphid (glyph)
  "Read the field from a GLYPH that terminals use for display."
  (lglyph-char glyph))

(defsubst lglyph-set-termglyphid (glyph glyphid)
  "Set the field in a GLYPH that terminals use for display."
  (lglyph-set-char glyph glyphid))

(defsubst lglyph-diacritic-p (glyph)
  (eq 'Mn (get-char-code-property
           (lglyph-termglyphid glyph) 'general-category)))

(defun lgstring-merge-glyphs (gstring idx1 idx2)
  "Merge the two glyphs given by IDX1 and IDX2 in GSTRING.
Requires that IDX1 < IDX2.  All the characters between those two
are supposed to be diacritics, and will be removed by this
function."
  ;; Fix up the first glyph with combined information for attributes
  ;; `from' and `to'.
  (let ((glyph1 (lgstring-glyph gstring idx1))
        (glyph2 (lgstring-glyph gstring idx2)))
    (lglyph-set-from-to glyph1 (lglyph-from glyph1) (lglyph-to glyph2)))
  ;; Delete the second glyph and anything in between.  FIXME: We'd
  ;; love to preserve the diacritics between idx1 and idx2 by moving
  ;; them after the combination, but we do not know how to do that
  ;; yet.  OTOH it's not clear that this would work anyway.
  (let ((end-glyph (1- (lgstring-glyph-len gstring)))
        last-set)
    (cl-loop for s from (1+ idx2) to end-glyph
             for d from (1+ idx1)
             do
             (let ((next (lgstring-glyph gstring s)))
               (lgstring-set-glyph gstring d next)
               (setq last-set d)
               (when (null next)
                 (cl-return))))
    (cl-loop for i from (1+ last-set) to end-glyph
             do (lgstring-set-glyph gstring i nil))))

;;; A primitive to determine if a character is interesting for us.

(defsubst termshape-arabic-p (char)
  "A predicate that returns t for all Arabic characters."
  (aref termshape-arabic-chars char))

;;; Primitive to find out about ligatures.

(defun termshape-ligate (a b)
  "Make a ligature of two characters.
Return a precomposed character or nil, if there is no such
ligature."
  (let ((atab (aref termshape-ligatures a)))
    (if atab (aref atab b) nil)))

;;; Primitives to find out about shaping variants.

(defsubst termshape-shape-connect-next (c)
  "Find the character variant that connects forward."
  (aref termshape-shapes-connect-next c))

(defsubst termshape-shape-connect-prev (c)
  "Find the character variant that connects backward."
  (aref termshape-shapes-connect-prev c))

;;; High-level functions.

(defun termshape-ligate-gstring (gstring)
  "Replace all known ligatures in GSTRING."
  (cl-loop for i from 0 to (- (lgstring-glyph-len gstring) 2)
           do
           (if (not (lgstring-glyph gstring i))
               (cl-return))
           (termshape-ligate-gstring-at gstring i)))

(defun termshape-ligate-gstring-at (gstring first)
  "Ligate the glyph at FIRST with all possible after in GSTRING.
Drop all diacritics between the characters, unless they are
supported by ligatures."
  (cl-loop for i from (1+ first) to (1- (lgstring-glyph-len gstring))
           with curr = (lgstring-glyph gstring first)
           with curr-glyphid = (lglyph-termglyphid curr)
           do
           (let ((next (lgstring-glyph gstring i)))
             (if (null next)
                 (cl-return))
             (let ((ligature (termshape-ligate
                              curr-glyphid (lglyph-termglyphid next))))
               (cond (ligature
                      (lglyph-set-termglyphid curr ligature)
                      (lgstring-merge-glyphs gstring first i)
                      ;; Stay on this glyph index and try again.  This
                      ;; is where chaining of ligatures is supported.
                      (setq curr-glyphid ligature)
                      (setq i (1- i)))
                     ((lglyph-diacritic-p next)) ;; Continue
                     (t (cl-return)))))))        ;; Stop.

(defun termshape-drop-diacritics-from-gstring (gstring)
  "Drop non-spacing marks from GSTRING."
  ;; "from 1": Assume that the first character is a base character.
  ;; We do not want to add extra code to `lgstring-merge-glyphs' for
  ;; deleting the first character, when that situation should never
  ;; occur in valid text anyway.
  (cl-loop for i from 1 to (1- (lgstring-glyph-len gstring))
           do
           (let ((curr (lgstring-glyph gstring i)))
             (if (null curr)
                 (cl-return))
             (when (lglyph-diacritic-p curr)
               (lgstring-merge-glyphs gstring (1- i) i)
               ;; Stay on this glyph index and try again.
               (setq i (1- i))))))

(defun termshape-shape-gstring (gstring)
  "Apply shaping rules for GSTRING."
  (cl-loop for i from 0 to (- (lgstring-glyph-len gstring) 2)
           do
           (if (null (lgstring-glyph gstring i))
               (cl-return))
           (termshape-shape-gstring-at gstring i)))

(defun termshape-shape-gstring-at (gstring first)
  "Apply shaping rules for GSTRING at FIRST and the next after."
  (cl-loop for i from (1+ first) to (1- (lgstring-glyph-len gstring))
           with curr = (lgstring-glyph gstring first)
           with curr-connected = (termshape-shape-connect-next
                                  (lglyph-termglyphid curr))
           do
           (if (null curr-connected)
               (cl-return))
           (let ((next (lgstring-glyph gstring i)))
             (cond ((null next)
                    (cl-return))
                   ((lglyph-diacritic-p next)) ;; Continue.
                   (t (let ((next-connected (termshape-shape-connect-prev
                                             (lglyph-termglyphid next))))
                        (when next-connected
                          (lglyph-set-termglyphid curr curr-connected)
                          (lglyph-set-termglyphid next next-connected))
                        (cl-return)))))))

(defun termshape-ligate-and-shape-gstring (gstring)
  "Ligate and shape a given GSTRING.
Bundles all the termshape functionality.  Modifies its input.

If this is called for a Linux console or outside of redisplay,
drop all diacritics that are left after ligating."
  (termshape-debug "input" gstring)
  (termshape-ligate-gstring gstring)
  ;; On the Linux console, diacritics do not work unless we use
  ;; precomposed characters, so delete any that are still there at
  ;; this point.  `font-object' is bound in our caller
  ;; `auto-compose-chars'.  `font-object' is actually a frame object
  ;; here and from a frame object we can get the terminal type as
  ;; indicated by the initialization function that was used for that
  ;; particular terminal.
  (if (or (not (boundp 'font-object))
          (eq 'terminal-init-linux
              (terminal-parameter (frame-terminal font-object)
                                  'terminal-initted)))
      (termshape-drop-diacritics-from-gstring gstring))
  (termshape-shape-gstring gstring)
  (termshape-debug "output" gstring)
  gstring)

;;; Integrate our shaping mechanism into the redisplay engine.

(defun termshape-compose-gstring-for-terminal-filter (args)
  "Advice filter to add shaping into redisplay engine on the terminal."
  (let ((gstring (car args)))
    (when (termshape-arabic-p (lglyph-termglyphid (lgstring-glyph gstring 0)))
      (termshape-ligate-and-shape-gstring gstring)))
  args)

;;;###autoload
(define-minor-mode termshape-mode
  "Activate arabic shaping on the terminal."
  :init-value nil
  :global t

  ;; We could check that the terminal supports Unicode in the first
  ;; place, but what would we gain?  There is no alternative to show
  ;; arabic reasonably at all if we do not have the glyphs we need.
  (if termshape-mode
      (add-function :filter-args
                    (symbol-function 'compose-gstring-for-terminal)
                    #'termshape-compose-gstring-for-terminal-filter)
    (remove-function (symbol-function 'compose-gstring-for-terminal)
                     #'termshape-compose-gstring-for-terminal-filter))

  ;; Since Emacs 26.
  (if (fboundp #'clear-composition-cache)
      (clear-composition-cache))
  (redisplay t))

;;; Announce us.

(provide 'termshape)

;;------------------------------------------------------------------------
;;      eof
;;------------------------------------------------------------------------
