;; -*-no-byte-compile: t; -*-
;;------------------------------------------------------------------------
;;      tshape-gen.el
;;
;;      Generate the data structures for termshape.el.  These
;;      functions are called only during byte-compiling termshape.el.
;;------------------------------------------------------------------------
;;      This file is part of Termshape.
;;
;;      Termshape is free software: You can redistribute it and/or
;;      modify it under the terms of the GNU General Public License as
;;      published by the Free Software Foundation, either version 3 of
;;      the License, or (at your option) any later version.
;;
;;      You should have received a copy of the GNU General Public
;;      License along with Termshape in the file COPYING.  If not, see
;;      <https://www.gnu.org/licenses/>.
;;------------------------------------------------------------------------

;; For `termshape-pp-char'.
(load (expand-file-name "tshape-utils.el" "."))

(defun termshape-make-ligature (ligatures a b r)
  "Record a ligature R for characters A and B in char-table LIGATURES."
  (let ((atab (aref ligatures a)))
    (when (null atab)
      (setq atab (make-char-table 'ligatures))
      (aset ligatures a atab))
    (aset atab b r)))

(defun termshape-make-future-ligature (a b r)
  "If B can be decomposed, return a list of ligature R, A and the decomposition.
Else return nil."
  (let ((decomp-b (get-char-code-property b 'decomposition)))
    (if (> (length decomp-b) 1)
        (append (list r a) decomp-b))))

(defun termshape-make-shapes
    (shapes-next shapes-prev isolate initial medial final)
  "Record shaping variants in char-tables SHAPES-NEXT and SHAPES-PREV.
ISOLATED is actually the base character, because usually that has
the same form as the actual isolated variant, if the terminal
does not shape itself."
  (aset shapes-next isolate initial)
  (aset shapes-prev isolate final)
  ;; This transition can not happen in our algorithm.
  ;; (if initial
  ;;     (aset shapes-prev initial medial))
  (aset shapes-next final medial))

(defun termshape-make-tables ()
  "Setup the tables for ligatures and shaping from Unicode data."

  (let ((arabic-chars (make-char-table 'chars))
        (ligatures (make-char-table 'ligatures))
        (shapes-next (make-char-table 'shapings))
        (shapes-prev (make-char-table 'shapings))
        shapedefs
        more-ligatures)

    (set-char-table-range arabic-chars
                          '(?\u0600 . ?\u06FF) 'basic)
    (set-char-table-range arabic-chars
                          '(?\u0750 . ?\u07FF) 'supplement)
    (set-char-table-range arabic-chars
                          '(?\u08A0 . ?\u08FF) 'extended)
    (set-char-table-range arabic-chars
                          '(?\uFB50 . ?\uFDFF) 'presentation-forms-a)
    (set-char-table-range arabic-chars
                          '(?\uFE70 . ?\uFEFF) 'presentation-forms-b)

    ;; Create a simple list of the relevant information from
    ;; Unicode data, indexed by base character.
    (cl-loop for ch from ?\u0100 to ?\uFFFF
             with decomp
             with base
             with base1
             with base2
             with type
             with shapedef
             with entry
             do

             (setq decomp (get-char-code-property ch 'decomposition))
             ;; In this moment we look for one of these:
             ;;  (1575 1619)
             ;;  (isolated 1610)
             ;;  (initial 1610)
             ;;  (medial 1610)
             ;;  (final 1610)
             (when (and (listp decomp)
                        (setq type (nth 0 decomp))
                        (setq base (nth 1 decomp))
                        (aref arabic-chars base)
                        (or (memq type '(isolated initial medial final))
                            (numberp type)))

               (cond ((and (eq 2 (length decomp))
                           (setq base1 (nth 0 decomp))
                           (setq base2 (nth 1 decomp))
                           (numberp base1)
                           (numberp base1)
                           (aref arabic-chars base1))
                      ;; Index ligatures by string instead of character.
                      (setq base (format "%c%c" base1 base2))
                      (setq type nil)
                      (termshape-make-ligature ligatures base1 base2 ch)
                      (push (termshape-make-future-ligature base1 base2 ch)
                            more-ligatures))

                     ((and (eq 3 (length decomp))
                           (setq base1 (nth 1 decomp))
                           (setq base2 (nth 2 decomp))
                           ;; Various versions of lam + alif, also
                           ;; combinations with diacritics.  FIXME:
                           ;; lam + alif and diacritics in various
                           ;; positions and multiple diacritics in
                           ;; general.
                           (or (and (eq ?\u0644 base)
                                    (memq base2
                                          '(?\u0622 ?\u0623 ?\u0625 ?\u0627)))
                               (eq 'Mn (get-char-code-property
                                        base2 'general-category))))
                      ;; Index ligatures by string instead of character.
                      (setq base (format "%c%c" base1 base2))
                      ;; Add ligatures on the fly.
                      (when (eq 'isolated type)
                        (termshape-make-ligature ligatures base1 base2 ch)
                        (push (termshape-make-future-ligature base1 base2 ch)
                              more-ligatures))))

               ;; Create an assoc list of assoc lists.
               (when (or (stringp base)
                         (eq 2 (length decomp)))
                 (setq entry (assoc base shapedefs))
                 (setq shapedef (cdr entry))
                 (push (cons type ch) shapedef)
                 (if entry
                     (setcdr entry shapedef)
                   (push (cons base shapedef) shapedefs)))))

    ;; Unicode contains ligatures like for e.g. lam+alif+madda that
    ;; are recorded in the Unicode tables as lam+(alif+madda).  For
    ;; our ligature algorithm we need the form (lam+alif)+madda so
    ;; that we can create the ligatures step-by-step while reading the
    ;; decomposed character sequence.  Therefore while we record the
    ;; ligatures given by Unicode, we remember the full decomposition.
    ;; As soon as we have all ligatures given by Unicode, we record
    ;; the additional sequences.  We only expect ligatures for three
    ;; characters at this point, so limit the code to that case.
    (mapc (lambda (ligature)
            (unless (eq (length ligature) 4)
              (error "Only 4-element ligature records supported."))
            (let* ((r (nth 0 ligature))
                   (a (nth 1 ligature))
                   (b (nth 2 ligature))
                   (c (nth 3 ligature))
                   ;; The ligature for the first two characters must
                   ;; already exist, so just extract that ony blindly.
                   (ab (aref (aref ligatures a) b)))
              ;; Make a new ligature.  This chains two two-character
              ;; ligatures to give a three-character ligature.
              (termshape-make-ligature ligatures ab c r)
              (if (termshape-make-future-ligature ab c r)
                  (error "Only two levels of decomposition supported."))))
          (delq nil more-ligatures))

    ;; Create shaping lookup tables from the collected shaping info.
    (mapc (lambda (pair)
            (let* ((base     (car pair))
                   (variants (cdr pair))
                   (isolated (cdr (assq 'isolated variants)))
                   (initial  (cdr (assq 'initial  variants)))
                   (medial   (cdr (assq 'medial   variants)))
                   (final    (cdr (assq 'final    variants))))
              (unless (stringp base)
                (setq isolated base))
              (if (and isolated final)
                  (termshape-make-shapes
                   shapes-next shapes-prev
                   isolated initial medial final)
                (if (and (or isolated initial medial final)
                         ;; HAMZA: Always isolated.
                         (not (eq ?\u0621 base))
                         ;; TATWEEL combos: Always medial.
                         (not (and (stringp base)
                                   (eq ?\u0640 (elt base 0)))))
                    ;; This happens with U+0677 ("U"+HAMZA) and with
                    ;; some of the diacritic combinations.  FIXME: Why
                    ;; does "U"+HAMZA have no final form?  FIXME: Also
                    ;; check for weird combinations of medial and
                    ;; initial.
                    (message "No full shape set: %s - %s"
                             (if (numberp base)
                                 (termshape-pp-char base)
                               (mapconcat #'termshape-pp-char base " + "))
                             (list isolated initial medial final))))))
          shapedefs)

    ;; Add TATWEEL explicitly, because the above does not cover it
    ;; correctly.
    (termshape-make-shapes
     shapes-next shapes-prev
     ?\u0640 ?\u0640 ?\u0640 ?\u0640)

    (list arabic-chars ligatures shapes-next shapes-prev)))

(provide 'tshape-gen)

;;------------------------------------------------------------------------
;;      eof
;;------------------------------------------------------------------------
