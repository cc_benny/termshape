;; -*-no-byte-compile: t; -*-
;;------------------------------------------------------------------------
;;      tshape-debug.el
;;
;;      A simple add-on package for termshape.el to store debug info
;;      during redisplay without the risk of inf loops.
;;------------------------------------------------------------------------
;;      This file is part of Termshape.
;;
;;      Termshape is free software: You can redistribute it and/or
;;      modify it under the terms of the GNU General Public License as
;;      published by the Free Software Foundation, either version 3 of
;;      the License, or (at your option) any later version.
;;
;;      You should have received a copy of the GNU General Public
;;      License along with Termshape in the file COPYING.  If not, see
;;      <https://www.gnu.org/licenses/>.
;;------------------------------------------------------------------------

;; For `lgstring-to-string'.
(load (expand-file-name "tshape-utils.el" "."))

(defvar termshape-debug-log ""
  "Information collected by `termshape-debug-log' so far.")

(defun termshape-debug-log (tag gstring)
  "Add TAG and GSTRING to the debug text in `termshape-debug-entries'."
  (setq termshape-debug-log
        (format "%s%s: %s> [%d]:%s\n"
                termshape-debug-log
                tag
                (if (boundp 'font-object)
                    (frame-terminal font-object)
                  "")
                (lgstring-glyph-len gstring)
                (lgstring-to-string gstring))))

;;------------------------------------------------------------------------
;;      eof
;;------------------------------------------------------------------------

