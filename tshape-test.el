;; -*-no-byte-compile: t; -*-
;;------------------------------------------------------------------------
;;      tshape-test.el
;;------------------------------------------------------------------------
;;      This file is part of Termshape.
;;
;;      Termshape is free software: You can redistribute it and/or
;;      modify it under the terms of the GNU General Public License as
;;      published by the Free Software Foundation, either version 3 of
;;      the License, or (at your option) any later version.
;;
;;      You should have received a copy of the GNU General Public
;;      License along with Termshape in the file COPYING.  If not, see
;;      <https://www.gnu.org/licenses/>.
;;------------------------------------------------------------------------

(require 'ert)

;; Use `load' instead of `require' so we always get the current
;; version from this directory, not some installed version.
(load (expand-file-name "termshape.el" "."))

;; For `lgstring-to-string' and `lgstring-make'.
(load (expand-file-name "tshape-utils.el" "."))

(defun termshape-string-shape (string)
  "Run a string through our shaper."
  (let ((gstring (lgstring-make string)))
    (termshape-ligate-and-shape-gstring gstring)
    (lgstring-to-string gstring)))

(ert-deftest termshape-test-non-arabic ()
  "Leave non-Arabic alone."
  (should (equal (termshape-string-shape
                  "abc")
                 "abc")))

(ert-deftest termshape-test-alef-and-hamza-input-as-ligature ()
  "\u0623 = ALEF and HAMZA ABOVE, plus context."
  (should (equal (termshape-string-shape
                  "\u0623\u0628\u0648")
                 "\u0623\uFE91\uFEEE")))

(ert-deftest termshape-test-alef-and-hamza-composition ()
  "\u0623 = \u0627\u0654, composition."
  (should (equal (termshape-string-shape
                  "\u0627\u0654\u0628\u0648")
                 "\u0623\uFE91\uFEEE")))

(ert-deftest termshape-test-drop-shaddah ()
  "\u0651 = SHADDAH - Drop this non-spacing mark."
  (should (equal (termshape-string-shape
                  "\u0627\u062A\u0651\u062D\u0627\u062F")
                 "\u0627\uFE97\uFEA4\uFE8E\u062F")))

(ert-deftest termshape-test-alef-and-fathatan ()
  "\u0627\u064B = ALEF + FATHATAN -> \uFD3C."
  (should (equal (termshape-string-shape
                  "\u0623\u062D\u064A\u0627\u0646\u0627\u064B")
                 "\u0623\uFEA3\uFEF4\uFE8E\uFEE7\uFD3C")))

(ert-deftest termshape-test-lam-alef-and-hamza ()
  "\u0644\u0623 = LAM + (ALEF + HAMZA) -> \uFEF7."
  (should (equal (termshape-string-shape
                  "\u0627\u0644\u0623\u0643\u062B\u0631")
                 "\u0627\uFEF7\uFEDB\uFE9C\uFEAE")))

(ert-deftest termshape-test-lam-alef-and-hamza ()
  "\u0644\u0623 = \u0644\u0627\u0654 = LAM + ALEF + HAMZA -> \uFEF7."
  (should (equal (termshape-string-shape
                  "\u0627\u0644\u0627\u0654\u0643\u062B\u0631")
                 "\u0627\uFEF7\uFEDB\uFE9C\uFEAE")))

(ert-deftest termshape-test-lam-alef-and-hamza-partially-precomposed ()
  "\u0644\u0623 = \uFEFB\u0654 = (LAM + ALEF) + HAMZA -> \uFEF7."
  (should (equal (termshape-string-shape
                  "\u0627\uFEFB\u0654\u0643\u062B\u0631")
                 "\u0627\uFEF7\uFEDB\uFE9C\uFEAE")))

(ert-deftest termshape-test-lam-fatha-alef ()
  "\u0644\u064E\u0627 = LAM + FATHA + ALEF -> (LAM + ALEF)."
  (should (equal (termshape-string-shape
                  "\u0644\u064E\u0627")
                 "\uFEFB")))

(ert-deftest termshape-test-space-stops-shaping ()
  "\u0637 \u0627 -> \u0637 \u0627."
  (should (equal (termshape-string-shape
                  "\u0637 \u0627")
                 "\u0637 \u0627")))

;;------------------------------------------------------------------------
;;      eof
;;------------------------------------------------------------------------
