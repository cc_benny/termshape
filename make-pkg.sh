#!/bin/bash
# ------------------------------------------------------------------------
#   make-pkg.sh
#
#   Make snapshot package (a tar file) with the current date as the
#   version number.
# ------------------------------------------------------------------------
#   This file is part of Termshape.
#
#   Termshape is free software: You can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published
#   by the Free Software Foundation, either version 3 of the License,
#   or (at your option) any later version.
#
#   You should have received a copy of the GNU General Public License
#   along with Termshape in the file COPYING.  If not, see
#   <https://www.gnu.org/licenses/>.
# ------------------------------------------------------------------------

set -e  # Exit on error.

# Go to where the files are.
cd $(dirname "$0")

if [ -n "$1" ]; then
    VERSION="$1"
else
    VERSION=$(sed -n '/VERSION/ { s/.*"\([^"]*\)".*/\1/g; p}' < termshape-pkg.el)
    # XXXXsnapshotXXXX: This is how package.el wants it.
    VERSION+=snapshot$(date +%Y%m%d%H%M)
fi
STAGE=build/termshape-$VERSION

mkdir -p $STAGE
cp *.el *.md COPYING $STAGE
sed "/VERSION/ s/\"\\([^\"]*\\)\"/\"$VERSION\"/g" \
    < termshape-pkg.el > $STAGE/termshape-pkg.el
tar cvzf $STAGE.tar.gz -C $(dirname $STAGE) $(basename $STAGE)

echo ""
echo "Built package as $STAGE.tar.gz"

# ------------------------------------------------------------------------
#   eof
# ------------------------------------------------------------------------
