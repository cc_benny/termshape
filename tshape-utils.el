;; -*-no-byte-compile: t; -*-
;;------------------------------------------------------------------------
;;      tshape-utils.el
;;
;;      Stuff that is optional, used for debugging, in the tests or by
;;      tshape-gen.el but not in termshape.el itself.
;;------------------------------------------------------------------------
;;      This file is part of Termshape.
;;
;;      Termshape is free software: You can redistribute it and/or
;;      modify it under the terms of the GNU General Public License as
;;      published by the Free Software Foundation, either version 3 of
;;      the License, or (at your option) any later version.
;;
;;      You should have received a copy of the GNU General Public
;;      License along with Termshape in the file COPYING.  If not, see
;;      <https://www.gnu.org/licenses/>.
;;------------------------------------------------------------------------

(defun lgstring-make (string)
  "Construct a valid terminal gstring from STRING."
  (composition-get-gstring 0 (length string) nil
                           (string-to-multibyte string)))

(defun lgstring-to-string (gstring)
  "Extract the characters from a GSTRING and format a string."
  (cl-loop for i from 0 to (lgstring-glyph-len gstring)
           with lglyph
           with string = ""
           finally return string
           do
           (setq lglyph (lgstring-glyph gstring i))
           (if (null lglyph)
               (cl-return string))
           (setq string (concat string (char-to-string
                                        (lglyph-termglyphid lglyph))))))

(defun termshape-pp-char (character)
  "Generate a verbose string for a given CHARACTER."
  (format "U+%04X (%s)" character (get-char-code-property character 'name)))

(defun termshape-pp-table (table)
  "Generate a string representation for one of our char-tables.
Does not do any indentation yet."
  (let (result (first t))
    (map-char-table
     (lambda (key value)
       (when (not first)
         (setq result (concat result "\n")))
       (setq first nil)
       (setq result (concat result "(" (termshape-pp-char key)))
       (setq result (concat result "\n"))
       (cond ((char-table-p value)
              (setq result (concat result (termshape-pp-table value))))
             ((numberp value)
              (setq result (concat result (termshape-pp-char value))))
             (t
              (setq result (concat result (format "%s" value)))))
       (setq result (concat result ")")))
     table)
    result))

(defun termshape-dump ()
  "Generate a buffer with representations of all of our char-tables."
  (interactive)
  (require 'termshape)
  (switch-to-buffer (get-buffer-create "*Termshape-Dump*"))
  (lisp-mode)
  (delete-region (point-min) (point-max))
  (let ((mark (make-marker)))
    (insert "Ligatures\n\n")
    (setq mark (set-marker mark (point)))
    (insert (termshape-pp-table termshape-ligatures))
    (indent-region mark (point))
    (insert "\n\nNext\n\n")
    (setq mark (set-marker mark (point)))
    (insert (termshape-pp-table termshape-shapes-connect-next))
    (indent-region mark (point))
    (insert "\n\nPrev\n\n")
    (setq mark (set-marker mark (point)))
    (insert (termshape-pp-table termshape-shapes-connect-prev))
    (indent-region mark (point))
    (setq mark (set-marker mark nil))
    (insert "\n")))

;;------------------------------------------------------------------------
;;      eof
;;------------------------------------------------------------------------

