;;------------------------------------------------------------------------
;;      termshape-pkg.el
;;------------------------------------------------------------------------
;;      This file is part of Termshape.
;;
;;      Termshape is free software: You can redistribute it and/or
;;      modify it under the terms of the GNU General Public License as
;;      published by the Free Software Foundation, either version 3 of
;;      the License, or (at your option) any later version.
;;
;;      You should have received a copy of the GNU General Public
;;      License along with Termshape in the file COPYING.  If not, see
;;      <https://www.gnu.org/licenses/>.
;;------------------------------------------------------------------------

(define-package
  "termshape"
  "1.0" ; VERSION - This string is extended by make-pkg.sh.
  "Shape Arabic characters in terminal mode.")

;;------------------------------------------------------------------------
;;      eof
;;------------------------------------------------------------------------

