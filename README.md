[//]: # (README.md)

Termshape
====

Termshape is an Emacs package that implements Arabic shaping in a
terminal.

Emacs' OOTB behaviour, when it displays Arabic script in a terminal,
i.e. when it is invoked with the ``-nw`` switch, or when it was
compiled without windowing support, is to perform the bidi algorithm
but not to perform shaping.

This package adds simple shaping, based on the Arabic presentation
forms available with standard Unicode support.  For more discussion of
the implementation, see the file ``overview.md``.

The package has been tested with Emacs 25, 26, 27 and 28 on the Linux
console (using the fonts from the [Bicon project),
](http://github.com/behdad/bicon) on FbTerm and on the Gnome terminal.

Like Emacs, this software is licensed under the [GPL3 or
later](https://www.gnu.org/licenses/), see also the file
[`COPYING`](COPYING).

Problems and work-arounds
----

#### gnome-terminal

After version 3.33.3, the built-in bidi processing needs to be
disabled by issuing `printf "\e[8l"` [(see announcement)
](https://mail.gnome.org/archives/gnome-announce-list/2019-July/msg00003.html)
or doing equivalent output.  gnome-terminal will do shaping with that
setting, but ligatures like lam-alif still require this package.

#### Linux console

Since Emacs 28, `auto-composition-mode` is disabled here, which
disables this package, too.  A solution has yet to be found, probably
by configuring auto-composition in more detail.

#### FbTerm

The `TERM` environment variable has to be set to `fbterm` or at least
something different from `linux`, because `linux` triggers the Linux
console handling, as mentioned in the previous section.

Ideas
----

Can auto-composition be enhanced to do the job of this package?

----
[//]: # (eof)
